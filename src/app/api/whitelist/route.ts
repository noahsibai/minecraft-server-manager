import { MISSING_USERNAME } from "@/constants";
import { NextRequest, NextResponse } from "next/server";

const BASE_API_URL: string = process.env.BASE_URL as string;
const KEY_HEADER: string = process.env.SECRET_KEY as string;

const authHeaders = { key: KEY_HEADER };

const WHITELIST_URL = `${BASE_API_URL}/server/whitelist`;

export async function GET(): Promise<
  NextResponse<WhitelistedUsers | APIError>
> {
  const getWhitelistedUsers: WhitelistedUsername[] | APIError = await fetch(
    WHITELIST_URL,
    {
      headers: authHeaders,
    },
  )
    .then((response) => {
      if (response.status != 200) {
        return { error: `${response.status} - ${response.statusText}` };
      }
      return response.json();
    })
    .catch((error) => {
      return { error: error };
    });

  if (
    typeof getWhitelistedUsers === "object" &&
    !Array.isArray(getWhitelistedUsers) &&
    getWhitelistedUsers !== null
  ) {
    return NextResponse.json(getWhitelistedUsers, { status: 400 });
  }

  let whitelistedUsernames: WhitelistedUsers;

  if (Array.isArray(getWhitelistedUsers)) {
    whitelistedUsernames = {
      users: getWhitelistedUsers
        .map((user: WhitelistedUsername) => {
          return user.name;
        })
        .sort((a, b) => a.localeCompare(b)),
    };
  }

  return NextResponse.json(whitelistedUsernames!);
}

export async function POST(
  req: NextRequest,
): Promise<NextResponse<APIStatus | APIError>> {
  let formData: FormData;

  try {
    formData = await req.formData();
  } catch (error) {
    return NextResponse.json({ error: MISSING_USERNAME }, { status: 400 });
  }

  let username = formData.get("name")?.toString();

  if (!username) {
    return NextResponse.json({ error: MISSING_USERNAME }, { status: 400 });
  }

  const form = new FormData();

  form.append("name", username);

  const config: RequestInit = {
    body: form,
    headers: authHeaders,
    method: "POST",
  };

  const whitelistUser: string | APIError = await fetch(WHITELIST_URL, config)
    .then((response) => {
      if (response.status != 200) {
        return { error: `${response.status} - ${response.statusText}` };
      }
      return response.json();
    })
    .catch((error) => {
      return { error };
    });

  if (typeof whitelistUser !== "string") {
    return NextResponse.json(whitelistUser, { status: 400 });
  }

  return NextResponse.json({ status: whitelistUser }, { status: 202 });
}

export async function DELETE(
  req: NextRequest,
): Promise<NextResponse<APIStatus | APIError>> {
  let formData: FormData;

  try {
    formData = await req.formData();
  } catch (error) {
    return NextResponse.json({ error: MISSING_USERNAME }, { status: 400 });
  }

  const username = formData.get("name")?.toString();

  if (!username) {
    return NextResponse.json({ error: MISSING_USERNAME }, { status: 400 });
  }

  const form = new FormData();
  form.append("name", username);

  const config: RequestInit = {
    body: form,
    headers: authHeaders,
    method: "DELETE",
  };

  const deleteUser: string | APIError = await fetch(WHITELIST_URL, config)
    .then((response) => {
      if (response.status != 200) {
        return { error: `${response.status} - ${response.statusText}` };
      }
      return response.json();
    })
    .catch((error) => NextResponse.json({ error: error }, { status: 400 }));

  if (typeof deleteUser !== "string") {
    return NextResponse.json(deleteUser, { status: 400 });
  }

  return NextResponse.json({ status: deleteUser }, { status: 202 });
}

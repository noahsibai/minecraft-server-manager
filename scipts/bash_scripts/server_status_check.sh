#!/bin/bash

STARTUP_SCRIPT=$1
LOG_FILE=$2
SERVER_LOCATION=$3
API_ENDPOINT=$4
EXPECTED_RUNNING_PROCESS=$5

if [ "$#" -eq 5 ]; then
    PSOUTPUT=$(ps -aux | grep -i "$EXPECTED_RUNNING_PROCESS" | grep -v grep)
    PSOUTPUT=$(echo "$PSOUTPUT" | xargs)

    if [ "" = "$PSOUTPUT" ]; then
        DATE=$(date)
        # Sends a message to gotify
        curl $API_ENDPOINT -F "title=Server Status" -F "message=Minecraft Server is currnetly down on $DATE" -F "priority=5"
        echo "server is down at: " $DATE >>$LOG_FILE
        TMUX_PID=$(pidof tmux)
        kill -9 $TMUX_PID
        echo "attempting to start server at: " $DATE >>$LOG_FILE
        bash $STARTUP_SCRIPT $LOG_FILE $SERVER_LOCATION
    else
        echo "server is currently running at: " $DATE >>$LOG_FILE
    fi
else
    exit 1
fi

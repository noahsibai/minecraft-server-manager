#!/bin/bash

declare -a APPROVED_OVERWRITES=()
BASE_ENV=".env.example"
BASENAME=$(basename $0)
CURRENT_DIR=$(pwd)
ENVIRONMENTS=("dev" "local" "production")
OVERRIDE_CONFIRMATION='O'
RETRIES=0

verify_valid_response() {
  if [[ $RETRIES -gt 4 ]]; then
    echo -e "Too many wrong attempts\n"
    exit 1
  fi

  [[ "$1" =~ ^(y|Y|n|N)$ || "$1" == "" ]]
}

if [[ $# -eq 1 ]]; then
  if [[ $1 == "override" ]]; then
    OVERRIDE=true
  else
    echo -e "Usage: \n"
    echo $BASENAME - generates .env files without overriding
    echo $BASENAME help - useage/help message
    echo -e "$BASENAME override - generates .env files with override prompt\n"
    exit 0
  fi
fi

if [[ $OVERRIDE ]]; then
  echo You are about to overwrite the following env files:
  for env in ${ENVIRONMENTS[@]}; do
    if [[ ! -f ${CURRENT_DIR}/.env.${env} ]]; then
      APPROVED_OVERWRITES+=($env)
      continue
    fi
    echo -e "   - .env.$env"
    while ! verify_valid_response $OVERRIDE_CONFIRMATION; do
      read -p "Are you sure (y/N)? " OVERRIDE_CONFIRMATION
      RETRIES=$((1 + RETRIES))
    done

    RETRIES=0

    if [[ "$OVERRIDE_CONFIRMATION" =~ ^(y|Y)$ ]]; then
      APPROVED_OVERWRITES+=($env)
    fi

    OVERRIDE_CONFIRMATION='O'
  done

fi

declare -a OUTPUT_FILES=()

if [[ ${#APPROVED_OVERWRITES[@]} -eq 0 && $OVERRIDE ]]; then
  echo -e "Nothing to write. Exiting ...\n"
  exit 0
fi

for env in ${APPROVED_OVERWRITES[@]}; do
  if [[ ! -f "${CURRENT_DIR}/.env.${env}" || $OVERRIDE ]]; then
    cp ${CURRENT_DIR}/${BASE_ENV} ${CURRENT_DIR}/.env.${env}
    if [[ ! -f ${CURRENT_DIR}/.env.${env} ]]; then
      echo -e "Unable to create ${CURRENT_DIR}/.env.${env}\n"
      exit 1
    fi
    OUTPUT_FILES+=(.env.${env})
  fi
done

if [[ ${#OUTPUT_FILES[@]} -eq 0 ]]; then
  echo -e "All files are already present\n"
  exit 0
fi

echo -e "Generated the following files: ${OUTPUT_FILES[@]}\n"
exit 0

#!/bin/bash

LOG_FILE=$1
SERVER_LOCATION=$2
EXPECTED_RUNNING_PROCESS=$3

if [ "$#" -eq 3 ]; then
    PSOUTPUT=$(ps -aux | grep -i "$EXPECTED_RUNNING_PROCESS" | grep -v grep)
    PSOUTPUT=$(echo "$PSOUTPUT" | xargs)

    if [ "" = "$PSOUTPUT" ]; then
        tmux new-ses -d -t BACKGROUND
        tmux send-keys -t BACKGROUND-0 "echo 'starting: ' $(date) >> $LOG_FILE" ENTER
        tmux send-keys -t BACKGROUND-0 "cd $SERVER_LOCATION" ENTER
        tmux send-keys -t BACKGROUND-0 "$EXPECTED_RUNNING_PROCESS" ENTER
    fi
else
    echo "Not enough arguments passed"
    exit 1
fi

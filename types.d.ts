interface WhitelistedUsername {
  uuid?: string;
  name: string;
}

interface WhitelistedUsers {
  users: string[];
}

interface APIError {
  error: string;
}

interface APIStatus {
  status: string;
}
